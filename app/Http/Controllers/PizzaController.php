<?php

namespace App\Http\Controllers;

use App\Repositories\PizzaRepository;
use Illuminate\Http\Request;

class PizzaController extends Controller
{
    /**
     * @var PizzaRepository
     */
    private $pizzaRepository;

    /**
     * PizzaController constructor.
     * @param PizzaRepository $pizzaRepository
     */
    public function __construct(PizzaRepository $pizzaRepository)
    {
        $this->pizzaRepository = $pizzaRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $pizzas = $this->pizzaRepository->getList();
        return view('pizza.index', compact('pizzas'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $pizza = $this->pizzaRepository->show($id);
        return view('pizza.show', compact('pizza'));
    }
}
