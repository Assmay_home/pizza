<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\OrderStoreRequest;
use App\Models\Order;
use App\Models\OrderPosition;
use App\Repositories\OrderRepository;
use App\Repositories\PizzaRepository;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->except(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param OrderRepository $orderRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(OrderRepository $orderRepository)
    {
        return response()->json($orderRepository->getList());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param OrderStoreRequest $request
     * @param PizzaRepository $pizzaRepository
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(OrderStoreRequest $request, PizzaRepository $pizzaRepository)
    {
        $data = $request->only(['address', 'cart']);
        try {
            $order = new Order();
            $positions = collect();
            foreach ($data['cart'] as $item) {
                $position = new OrderPosition();
                $pizza = $pizzaRepository->find($item['id']);
                if ($pizza){
                    $position->product()->associate($pizza);
                    $position->quantity = $item['quantity'];
                    $position->size = $item['size'];
                    $positions->push($position);
                }
            }
            if ($positions->isNotEmpty()){
                $order->address = $data['address'];
                $order->user()->associate(\Auth::user());
                $order->save();
                $order->positions()->saveMany($positions);
            }else{
                throw new \Exception('Undefined positions. Can\'t find pizza.');
            }
        }catch (\Exception $exception){
            if ($order)
                $order->delete();
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ]);
        }
        return response()->json([
            'success' => true,
            'order_id' => $order->id
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @param OrderRepository $orderRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id, OrderRepository $orderRepository)
    {
        $order = $orderRepository->show($id);
        if ($order)
            return response()->json($order);

        return response()->json(['success' => false, 'message' => 'Not Found!'], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @param OrderRepository $orderRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id, OrderRepository $orderRepository)
    {
        $data = $request->only(['status']);
        if (!isset(Order::getOrderStatuses()[$data['status']]))
            return response()->json(['success' => false, 'message' => 'wrong status'], 500);
        $order = $orderRepository->find($id);
        try {
            $result = $order->update($data);
            return response()->json(['success' => $result]);
        }catch (\Exception $exception){
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ], $exception->getCode());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @param OrderRepository $orderRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id, OrderRepository $orderRepository)
    {

        $order = $orderRepository->find($id);
        if ($order->status != 3)
            return response()->json(['success' => false, 'message' => 'you can\'t delete order on this status'], 403);
        try {
            $result = $order->delete();
            return response()->json(['success' => $result]);
        }catch (\Exception $exception){
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ], $exception->getCode());
        }
    }
}
