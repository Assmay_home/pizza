<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\PizzaRequest;
use App\Models\Pizza;
use App\Repositories\PizzaRepository;
use Illuminate\Http\Request;

class PizzaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->except(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param PizzaRepository $pizzaRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(PizzaRepository $pizzaRepository)
    {
        return response()->json($pizzaRepository->getList());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PizzaRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PizzaRequest $request)
    {
        $data = $request->only(['name', 'slug']);
        try {
            $pizza = new Pizza($data);
            $pizza->save();
        }catch (\Exception $exception){
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ]);
        }
        return response()->json([
            'success' => true,
            'pizza_id' => $pizza->id
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @param PizzaRepository $pizzaRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id, PizzaRepository $pizzaRepository)
    {
        $pizza = $pizzaRepository->show($id);
        if (!$pizza)
            return response()->json(['success' => false, 'message' => 'Not Found!'], 404);

        return response()->json($pizza);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PizzaRequest $request
     * @param int $id
     * @param PizzaRepository $pizzaRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(PizzaRequest $request, $id, PizzaRepository $pizzaRepository)
    {
        $data = $request->only(['name', 'slug']);
        $pizza = $pizzaRepository->find($id);
        if (!$pizza)
            return response()->json(['success' => false, 'message' => 'Not Found!'], 404);

        try {
            $result = $pizza->update($data);
            return response()->json(['success' => $result]);
        }catch (\Exception $exception){
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ], $exception->getCode());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @param PizzaRepository $pizzaRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id, PizzaRepository $pizzaRepository)
    {
        $pizza = $pizzaRepository->find($id);

        try {
            $result = $pizza->delete();
            return response()->json(['success' => $result]);
        }catch (\Exception $exception){
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ], $exception->getCode());
        }
    }
}
