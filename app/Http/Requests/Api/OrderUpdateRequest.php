<?php

namespace App\Http\Requests\Api;

class OrderUpdateRequest extends OrderRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status' => ['required', 'integer']
        ];
    }
}
