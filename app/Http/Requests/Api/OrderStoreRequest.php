<?php

namespace App\Http\Requests\Api;

class OrderStoreRequest extends OrderRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address' => ['required', 'string'],
            'cart' => ['required', 'array'],
        ];
    }
}
