<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pizza extends Model
{
    protected $fillable = ['name', 'slug'];
    public function position()
    {
        return $this->morphOne(OrderPosition::class, 'product');
    }

    public static function getPizzaSizes()
    {
        return [
            1 => 'Small',
            2 => 'Middle',
            3 => 'Big',
        ];
    }
}
