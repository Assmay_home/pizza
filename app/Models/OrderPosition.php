<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderPosition extends Model
{
    protected $fillable = [
        'quantity',
        'size',
    ];

    public function product()
    {
        return $this->morphTo();
    }
}
