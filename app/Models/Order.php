<?php

namespace App\Models;

use App\Traits\Jsonable;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['address', 'status', 'cart'];

    protected $casts = [
        'cart' => 'json'
    ];

    protected static function boot()
    {
        parent::boot();


        static::deleting(function($order) {
            $order->positions()->delete();
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function positions()
    {
        return $this->hasMany(OrderPosition::class);
    }

    public static function getOrderStatuses()
    {
        return [
            1 => 'New',
            2 => 'Prepare',
            3 => 'Delivered',
        ];
    }
}
