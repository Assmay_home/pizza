<?php


namespace App\Repositories;


use App\Models\Pizza as Model;

class PizzaRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    public function getList()
    {
        $result = \Cache::rememberForever('pizza-list', function () {
            return $this->startConditions()->all();
        });
        return $result;
    }

    public function find(int $id)
    {
        $result = $this->startConditions()->select(['id'])->find($id);
        return $result;
    }

    public function show(int $id)
    {
        return $this->startConditions()->find($id);
    }
}
