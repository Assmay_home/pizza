<?php


namespace App\Repositories;


use App\Models\Order as Model;

class OrderRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    public function getList()
    {
        if ($user = \Auth::user())
            return $this->startConditions()->where('user_id', $user->id)->get();
        return $this->startConditions()->all();
    }

    public function find(int $id)
    {
        $result = $this->startConditions()->find($id);
        return $result;
    }

    public function show(int $id)
    {
        return $this->startConditions()->with('positions.product')->find($id);
    }
}
