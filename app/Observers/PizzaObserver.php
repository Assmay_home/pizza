<?php

namespace App\Observers;

use App\Models\Pizza;

class PizzaObserver
{
    public function saved(Pizza $pizza)
    {
        self::sluggable($pizza);
    }

    public function saving(Pizza $pizza)
    {
        self::sluggable($pizza);
    }

    private static function sluggable(Pizza &$pizza)
    {
        if (empty($pizza->slug))
            $pizza->slug = \Str::slug($pizza->name);
    }
}
