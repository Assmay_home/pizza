<?php

namespace App\Providers;

use App\Models\Pizza;
use App\Observers\PizzaObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Pizza::observe(PizzaObserver::class);
    }
}
