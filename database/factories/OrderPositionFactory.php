<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\OrderPosition;
use App\Models\Pizza;
use Faker\Generator as Faker;

$factory->define(OrderPosition::class, function (Faker $faker)  use ($factory){
    return [
        'size' => Pizza::getPizzaSizes()[rand(1,3)],
        'quantity' => rand(1,10),
        'product_id' => factory(Pizza::class)->create()->id,
        'product_type' => Pizza::class,
    ];
});
