<?php

use App\Models\OrderPosition;
use App\User;
use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Order::class, 10)->create([
            'user_id' => User::first()->id
        ])->each(function ($order){
            $positions = factory(OrderPosition::class, 5)->make();
            $order->positions()->saveMany($positions);
        });
    }
}
