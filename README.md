**Deploy on host:** <br>
On host you need nginx mysql and php
to up this code you need run follow commands:

git clone https://gitlab.com/Assmay_home/pizza.git .
composer install
<pre>
cp .env.example .env //enter your db connection data on DB_* section

php artisan key:generate
php artisan config:cache
php artisan migrate
php artisan passport:install
php artisan sleepingowl:install
</pre>


**Deploy on docker-compose.yml:** <br>
<pre>
git clone https://gitlab.com/Assmay_home/pizza.git {project_name}
cd {project_name}
docker run --rm -v $(pwd):/app composer install
sudo chown -R $USER:$USER ~/{project_name}

cp .env.example .env

docker-compose up -d

docker-compose exec db bash
mysql -u root -p //your_mysql_root_password
mysql> GRANT ALL ON laravel.* TO 'laraveluser'@'%' IDENTIFIED BY 'my_password';
mysql> FLUSH PRIVILEGES;
mysql> EXIT;
exit

docker-compose exec app vim .env

APP_URL=http://0.0.0.0:8080
DB_CONNECTION=mysql
DB_HOST=db
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=laraveluser
DB_PASSWORD=my_password

docker-compose exec app php artisan key:generate
docker-compose exec app php artisan config:cache


docker-compose exec app php artisan migrate

docker-compose exec app php artisan passport:install

docker-compose exec app php artisan sleepingowl:install
</pre>


API: <br>
For api auth used laravel/passport

POST      | api/register
<pre>
{"name": "your_name", "email": "your_email", "password": "your_password"}
</pre>
POST      | api/login
<pre>
{"email": "your_email", "password": "your_password"}
</pre>
POST      | api/logout<br>
POST      | api/orders			|	orders.store
<pre>
{
	"address":"your_address",
	"cart":[
		{
			"id":1,
			"quantity":1,
			"size":"Big"
		},
		{
			"id":2,
			"quantity":1,
			"size":"Small"
		},
		{
			"id":3,
			"quantity":1,
			"size":"Middle"
		}
	]
}
</pre>
GET|HEAD  | api/orders			|	orders.index<br>
DELETE    | api/orders/{order}	        |	orders.destroy<br>
PUT|PATCH | api/orders/{order}	        |	orders.update
<pre>
{"status":2}
</pre>
GET|HEAD  | api/orders/{order}	        |	orders.show<br>
GET|HEAD  | api/pizzas			|	pizzas.index<br>
POST      | api/pizzas			|	pizzas.store
<pre>
[
    {
        "name": "Pizza first",
        "slug": "pizza-first-slug"
    },
    {
        "name": "Pizza second",
        "slug": "pizza-second-slug"
    },
    {
        "name": "Pizza third",
        "slug": "pizza-third-slug"
    }
]
</pre>
DELETE    | api/pizzas/{pizza}  	|	pizzas.destroy<br>
PUT|PATCH | api/pizzas/{pizza}	        |	pizzas.update<br>
GET|HEAD  | api/pizzas/{pizza}	        |	pizzas.show
