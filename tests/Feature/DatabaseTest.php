<?php

namespace Tests\Feature;

use App\Models\Order;
use App\Models\OrderPosition;
use App\Models\Pizza;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DatabaseTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testDatabase()
    {
        $user = factory(User::class)->create([
            'email' => 'test@email.test'
        ]);
        $this->assertDatabaseHas('users',[
            'email' => 'test@email.test'
        ]);
        $pizzas = factory(Pizza::class, 3)->create([
            'name' => 'Test Pizza'
        ]);
        $this->assertDatabaseHas('pizzas',[
            'name' => 'Test Pizza'
        ]);
        $orders = factory(Order::class, 3)->create([
            'user_id' => factory(User::class)->create()->id
        ])->each(function ($order){
            $positions = factory(OrderPosition::class, 5)->make();
            $order->positions()->saveMany($positions);
        });
    }
}
