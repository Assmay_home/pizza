<?php

namespace Tests\Feature;

use App\Models\Pizza;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PizzaTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function testIndex()
    {

        factory(Pizza::class, 3)->create();

        $response = $this->get('api/pizzas');

        $response
            ->assertStatus(200)
            ->assertJsonStructure([[
                'id',
                'name',
                'slug',
            ]]);
    }

    public function testStore()
    {
        $data = ['name' => $this->faker->name, 'slug' => $this->faker->slug];
        $user = factory(User::class)->create();
        $response = $this->actingAs($user, 'api')
            ->json('POST','api/pizzas', $data);
        $response->assertStatus(200)
            ->assertJson([
                'success' => true
            ])
            ->assertJsonStructure([
                'success',
                'pizza_id'
            ]);
    }

    public function testShow()
    {
        $pizzas = factory(Pizza::class, 3)->create();

        $response = $this->get('api/pizzas/'.\Arr::random($pizzas->pluck('id')->toArray()));
        $response->assertStatus(200)
            ->assertJsonStructure([
                'id',
                'name',
                'slug',
            ]);
    }

    public function testUpdate()
    {
        $pizzas = factory(Pizza::class, 3)->create();
        $user = factory(User::class)->create();
        $response = $this->actingAs($user, 'api')->json('PUT', 'api/pizzas/'.\Arr::random($pizzas->pluck('id')->toArray()), [
            'name' => $this->faker->name,
            'slug' => $this->faker->slug
        ]);
        $response->assertStatus(200)
            ->assertJson([
                'success' => true
            ]);
    }

    public function testDestroy()
    {
        factory(Pizza::class)->create();
        $pizza = Pizza::first();
        $user = factory(User::class)->create();
        $response = $this->actingAs($user, 'api')->json('DELETE', 'api/pizzas/'.$pizza->id);
        $response->assertStatus(200)
            ->assertJson([
                'success' => true
            ]);
    }
}
