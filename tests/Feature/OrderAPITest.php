<?php

namespace Tests\Feature;

use App\Models\Order;
use App\Models\OrderPosition;
use App\Models\Pizza;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class OrderAPITest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function testIndex()
    {

        factory(Order::class, 3)->create([
            'user_id' => factory(User::class)->create()->id
        ])->each(function ($order){
            $positions = factory(OrderPosition::class, 5)->make();
            $order->positions()->saveMany($positions);
        });

        $response = $this->get('api/orders');

        $response
            ->assertStatus(200)
            ->assertJsonStructure([[
                'id',
                'user_id',
                'address',
                'status',
            ]]);
    }

    public function testStore()
    {
        $pizzas = factory(Pizza::class, rand(1,3))->create();
        $data = [];
        $pizzas->each(function ($pizza) use(&$data){
            $data['cart'][] = [
                'id' => $pizza->id,
                'quantity' => rand(1,10),
                'size' => Pizza::getPizzaSizes()[rand(1,3)],
            ];
        });
        $data['address'] = $this->faker->address;
        $user = factory(User::class)->create();
        $response = $this->actingAs($user, 'api')
            ->json('POST','api/orders', $data);
        $response->assertStatus(200)
            ->assertJson([
                'success' => true
            ])
            ->assertJsonStructure([
                'success',
                'order_id'
            ]);
    }

    public function testShow()
    {
        $orders = factory(Order::class, 3)->create([
            'user_id' => factory(User::class)->create()->id
        ])->each(function ($order){
            $positions = factory(OrderPosition::class, 5)->make();
            $order->positions()->saveMany($positions);
        });

        $response = $this->get('api/orders/'.\Arr::random($orders->pluck('id')->toArray()));
        $response->assertStatus(200)
            ->assertJsonStructure([
                'id',
                'user_id',
                'address',
                'status',
                'positions' => [[
                    'id',
                    'product_type',
                    'product_id',
                    'order_id',
                    'quantity',
                    'size',
                    'product' => [
                        'id',
                        'name',
                        'slug',
                    ],
                ]],
            ]);
    }

    public function testUpdate()
    {
        $orders = factory(Order::class, 3)->create([
            'user_id' => factory(User::class)->create()->id
        ])->each(function ($order){
            $positions = factory(OrderPosition::class, 5)->make();
            $order->positions()->saveMany($positions);
        });
        $user = factory(User::class)->create();
        $response = $this->actingAs($user, 'api')->json('PUT', 'api/orders/'.\Arr::random($orders->pluck('id')->toArray()), ['status' => 2]);
        $response->assertStatus(200)
            ->assertJson([
                'success' => true
            ]);
        $response = $this->actingAs($user, 'api')->json('PUT', 'api/orders/'.\Arr::random($orders->pluck('id')->toArray()), ['status' => 'asdasd']);

        $response->assertStatus(500)
            ->assertJson([
                'success' => false
            ])
            ->assertJsonStructure([
                'success',
                'message'
            ]);
    }

    public function testDestroy()
    {
        factory(Order::class)->create([
            'status' => 3,
            'user_id' => factory(User::class)->create()->id
        ])->each(function ($order){
            $positions = factory(OrderPosition::class, 5)->make();
            $order->positions()->saveMany($positions);
        });
        $order = Order::first();
        $user = factory(User::class)->create();
        $response = $this->actingAs($user, 'api')->json('DELETE', 'api/orders/'.$order->id);
        $response->assertStatus(200)
            ->assertJson([
                'success' => true
            ]);

        factory(Order::class)->create([
            'status' => 2,
            'user_id' => factory(User::class)->create()->id
        ])->each(function ($order){
            $positions = factory(OrderPosition::class, 5)->make();
            $order->positions()->saveMany($positions);
        });
        $order = Order::first();
        $user = factory(User::class)->create();
        $response = $this->actingAs($user, 'api')->json('DELETE', 'api/orders/'.$order->id);
        $response->assertStatus(403)
            ->assertJson([
                'success' => false
            ])
            ->assertJsonStructure([
                'success',
                'message'
            ]);

    }
}
