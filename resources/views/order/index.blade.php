@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <h1>Order:</h1>
                    <ul>
                        @foreach($orders as $order)
                            <li>
                            <p><label>Order id:</label> <span>#{{$order->id}}</span></p>
                                <h2>Order positions:</h2>
                                <div class="order-positions">
                                    <ul>
                                        @foreach($order->positions as $position)
                                            <li>
                                                <p><label>Name:</label> <span>{{$position->product->name}}</span></p>
                                                <p><label>Size:</label> <span>{{$position->size}}</span></p>
                                                <p><label>Quantity:</label> <span>{{$position->quantity}}</span></p>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </li>
                            <hr>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
