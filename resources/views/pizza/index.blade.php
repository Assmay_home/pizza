@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    @foreach($pizzas as $pizza)
                        <p><label>Pizza:</label> <span>{{$pizza->name}}</span></p>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
